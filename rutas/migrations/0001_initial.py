# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Car',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('shortname', models.CharField(max_length=100)),
                ('plate', models.CharField(max_length=32)),
                ('current_latitude', models.DecimalField(max_digits=10, decimal_places=7)),
                ('current_longitude', models.DecimalField(max_digits=10, decimal_places=7)),
                ('owner', models.ForeignKey(related_name='cars', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Route',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='TransportationLine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'empty', max_length=60)),
                ('shortname', models.CharField(default=b'empty', max_length=100)),
                ('address', models.CharField(default=b'empty', max_length=100)),
                ('phone', models.CharField(default=b'empty', max_length=100)),
                ('pub_date', models.DateTimeField()),
                ('owner', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='WayPoint',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('latitude', models.DecimalField(max_digits=10, decimal_places=7)),
                ('longitude', models.DecimalField(max_digits=10, decimal_places=7)),
                ('route', models.ForeignKey(related_name='waypoints', to='rutas.Route')),
            ],
        ),
        migrations.AddField(
            model_name='route',
            name='transportline',
            field=models.ForeignKey(related_name='routes', to='rutas.TransportationLine'),
        ),
        migrations.AddField(
            model_name='car',
            name='transportline',
            field=models.ForeignKey(related_name='cars', to='rutas.TransportationLine'),
        ),
    ]
