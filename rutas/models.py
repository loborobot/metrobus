from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

class AbstractCompany(models.Model):
    name = models.CharField(max_length=60, default='empty')
    shortname = models.CharField(max_length=100, default='empty')
    address = models.CharField(max_length=100, default='empty')
    phone =  models.CharField(max_length=100, default='empty')
    pub_date = models.DateTimeField()
    
    class Meta:
        abstract = True

class TransportationLine(AbstractCompany):
    #owner = models.ForeignKey(User, related_name='lines')
    """
    Me indica el usuario que lineas de transporte puede ver
    """
    owner = models.OneToOneField(User)

    def __unicode__(self):
        return '%s (%s)' % (self.name, self.owner.username)

class Car(models.Model):
    transportline = models.ForeignKey(TransportationLine, related_name='cars')
    owner = models.ForeignKey(User, related_name='cars')
    shortname = models.CharField(max_length=100)
    plate = models.CharField(max_length=32)

    current_latitude = models.DecimalField(max_digits=10, decimal_places=7) 
    current_longitude = models.DecimalField(max_digits=10, decimal_places=7)
    
    def __unicode__(self):
        return '%s (%s)' % (self.shortname, self.plate, self.owner.username)

class Route(models.Model):
    transportline = models.ForeignKey(TransportationLine, related_name='routes')
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return '%s (%s)' % (self.transportline.name, self.pub_date)

class WayPoint(models.Model):
    route = models.ForeignKey(Route, related_name='waypoints')
    latitude = models.DecimalField(max_digits=10, decimal_places=7)         
    longitude = models.DecimalField(max_digits=10, decimal_places=7)

    def __unicode__(self):
        return '%s (%s)' % (self.route.name, self.latitude, self.longitude)
