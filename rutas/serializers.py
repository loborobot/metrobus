from django.contrib.auth.models import User

from rest_framework import serializers

from rutas.models import Route, WayPoint, Car, TransportationLine


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')

class RegistrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password')

# class WayPointSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = WayPoint
#         fields = ('latitude', 'longitude')

# class RouteSerializer(serializers.ModelSerializer):
# 	waypoints = WayPointSerializer(many=True, required=False)
#     class Meta:
#         model = Route
#         fields = ('name')

class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = ('shortname', 'plate','current_latitude', 'current_longitude')

class TransportationLineSerializer(serializers.ModelSerializer):
	#routes = RouteSerializer(many=True, required=False)
    class Meta:
        model = TransportationLine
        fields = ('name')
        
