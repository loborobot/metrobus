from datetime import datetime

# Django
from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.conf import settings

# REST Framework
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.reverse import reverse

# Provider OAuth2
from provider.oauth2.models import Client

# rutas App
from rutas.serializers import TransportationLineSerializer
from rutas.serializers import UserSerializer, CarSerializer
from rutas.models import Route, TransportationLine, Car

#def index(request):
#    return HttpResponse("peticion")

def index(request): 
    user = request.user
    response = render(request, 'index.html', {'debug': settings.DEBUG})
    return response

class Root(APIView):
    #permission_classes = (IsAuthenticated)
    permission_classes = ()

    @staticmethod
    def get(request):
        return Response({
            'empresas': reverse('empresas', request=request),
            'buses': reverse('buses', request=request),
        })

class RegistrationView(APIView):
    """ Allow registration of new users. """
    permission_classes = ()

    def post(self, request):
        serializer = RegistrationSerializer(data=request.DATA)

        # Check format and unique constraint
        if not serializer.is_valid():
            return Response(serializer.errors,\
                            status=status.HTTP_400_BAD_REQUEST)
        data = serializer.data

        u = User.objects.create(username=data['username'])
        u.set_password(data['password'])
        u.save()

        # Create OAuth2 client
        name = u.username
        client = Client(user=u, name=name, url='' + name,\
                client_id=name, client_secret='', client_type=1)
        client.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

class TransportationLineView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        """ Get all TransportationLine """
        trans = TransportationLine.objects.all()
        serializer = TransportationSerializer(trans, many=True)
        return Response(serializer.data)

class CarView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        """ Get all Cards """
        cards = Card.objects.all()
        serializer = CardSerializer(cards, many=True)
        return Response(serializer.data)

    def post(self, request):
        """ Adding a new todo. """
        serializer = CardSerializer(data=request.DATA)
        if not serializer.is_valid():
            return Response(serializer.errors, status=
                status.HTTP_400_BAD_REQUEST)
        else:
            data = serializer.data
            owner = request.user
            t = Card(owner=owner, shortname=data['shortname'], plate=['plate'])
            t.save()
            request.DATA['id'] = t.pk # return id
            return Response(request.DATA, status=status.HTTP_201_CREATED)

    def put(self, request, card_id):
        """ Update a card """
        serializer = CardSerializer(data=request.DATA)
        if not serializer.is_valid():
            return Response(serializer.errors, status=
                status.HTTP_400_BAD_REQUEST)
        else:
            data = serializer.data
            c = Card(id=card_id, owner=request.user, current_latitude=data['current_latitude'],\
                     current_longitude=data['current_longitude'])
            c.save()
            return Response(status=status.HTTP_200_OK)
