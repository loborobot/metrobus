from django.contrib import admin

from .models import Car, Route, TransportationLine
from .models import WayPoint

# Register your models here.
admin.site.register(Car)
admin.site.register(Route)
admin.site.register(TransportationLine)
admin.site.register(WayPoint)
