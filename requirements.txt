Django==1.8
django_oauth2_provider==0.2.6.1
djangorestframework==2.3.13
Markdown==2.6.2
django-filter==0.11.0
django-toolbelt==0.0.1
static3==0.5.1
wsgiref==0.1.2