# README #


### Configuraciones ###

* Configuration : revisar el archivo requerimets.txt
* Dependencias:
 * Python
 * Django-rest
 * Bower
 * Nodejs
 * SqlLite Data Base

## Pasos para configuracion local ###
 0. Configuracion
    git clone
    configurar local mente el archivo smartsensor/local_settings.py

 1. Instalar las dependencias en la raiz del proyecto
    cd web
    npm install 
 2. Crear un entorno virtual y instalar las dependencias
    
    source env/bin/activate
    
    pip install -r requeriments.txt

 2. Crear la DB localmente
    sudo su - postgres
    createdb smartcity
 
 3. sincronizar la base de datos
    python manage.py syncdb

### Pasos para instalacion en Produccion ###
  1. heroku create metrobus
  #heroku config:add BUILDPACK_URL=https://github.com/ddollar/heroku-buildpack-multi.git --app metrobus
  #3. heroku addons:add heroku-postgresql 
  4. git push heroku local_branch:master
  
  Nota: si se quiere eliminar una base de datos remota :
    heroku pg:reset DATABASE
### Version Testing ###
La version de este codigo ejecutandose en un servidor de produccion se puede encontrar en : http://smartsensor.herokuapp.com/

### Contacto ###

* Steve Ataucuri Cruz -> lord.ataucuri@ucsp.edu.pe