from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from rest_framework import viewsets, routers
from rutas import views

urlpatterns = patterns('',

    url(r'^$', views.index, name='root'),

	url(r'^api/', views.Root.as_view(), name='api'),
	 	
    # Registration of new users
    url(r'^api/register/$', views.RegistrationView.as_view()),

    # api endpoint
    #url(r'^api/$', views.TodosView.as_view()),
    url(r'^api/empresas/$', views.TransportationLineView.as_view(), name='empresas'),
    url(r'^api/empresas/(?P<id>[0-9]*)$', views.TransportationLineView.as_view(), name='empresas-detail'),
    url(r'^api/buses/$', views.CarView.as_view(), name='buses'),
    url(r'^api/buses/(?P<id>[0-9]*)$', views.CarView.as_view(), name='buses-detail'),

    #url(r'^api/points/$', views.WayPointView.as_view()),
    #url(r'^api/routes/$', views.RouteView.as_view(), name='routes'),
    #url(r'^api/routes/$', views.RouteViewG.as_view(), name='routes-detail'),

    url(r'^admin/', include(admin.site.urls)),

    # API authentication
    url(r'^oauth2/', include('provider.oauth2.urls', namespace='oauth2')),
    url(r'^api-auth/', include('rest_framework.urls',\
        namespace='rest_framework')),
)

#urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)